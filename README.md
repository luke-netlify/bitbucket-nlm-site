# bitbucket-nlm-site

This repo is used to simulate actions SSGs do with media during Netlify builds - without an SSG. This is the Bitbucket testing site for NLM for @mu.

It is used to test Netlify Large Media's behavior under various conditions.

When you do activate Large Media, here is the commands required to track the files:

```text
netlify lm:setup
git lfs track pre/green-river.png pre/large.jpg pre/logo.svg pre/test.svg
git add .
git commit -m 'enable Large Media'
git push
```

